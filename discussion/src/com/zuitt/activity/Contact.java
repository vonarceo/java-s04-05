package com.zuitt.activity;

import java.util.ArrayList;

public class Contact {
    // DECLARATION OF VARIABLE
    private String name;
    private String contactNumber;
    private String address;

    private Phonebook phonebook;


    // CONSTRUCTOR
    public Contact(){};
    public Contact(String name, String contactNumber, String address){
        this.name = name;
        this.address = address;
        this.contactNumber = contactNumber;

    }
    // GET
    public String getName(){
        return this.name;
    }
    public String getContactNumber(){
        return this.contactNumber;
    }

    public String getAddress() {
        return this.address;
    }



    // SET


    public void setName(String name) {
        this.name = name;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    // method

    public void printContact(){
        System.out.println("==========================================================");
        System.out.println(this.name);
        System.out.println(this.name + " has the following registered number: " + this.contactNumber);
        System.out.println(this.name + " has the following registered address: ");
        System.out.println("my home in " + this.address);
    }


}
