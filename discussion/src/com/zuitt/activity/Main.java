package com.zuitt.activity;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args){

    Contact firstContact = new Contact();
    Contact secondContact = new Contact();
    Contact thirdContact = new Contact();

    firstContact.setName("John Doe");
    firstContact.setContactNumber("+639152468596");
    firstContact.setAddress("Quezon City");

    secondContact.setName("Jane");
    secondContact.setContactNumber("+639162148573");
    secondContact.setAddress("Caloocan City");

    thirdContact.setName("Von");
    thirdContact.setContactNumber("+639153117784");
    thirdContact.setAddress("Paranaque");

      Phonebook phonebook = new Phonebook();

      phonebook.addContact(firstContact);
      phonebook.addContact(secondContact);
      phonebook.addContact(thirdContact);

        if(phonebook.getContact().size() == 0){
            System.out.println("Phonebook is Empty!");
        }
        else{
            ArrayList<Contact> contacts = phonebook.getContact();

            for(Contact contact : contacts){
                contact.printContact();
            }


        }



    }
}
