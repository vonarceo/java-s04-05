package com.zuitt.example;

import org.w3c.dom.ls.LSOutput;

import javax.lang.model.element.Name;

public class Car {

    //Access Modifier
        //These are used to restrict the scope of a class, constructor, variable, method or data member
        //Four type of access modifier.
            //default - (No Keyword indicated, within the package(com.zuitt.example))
            //Private - (Properties or method only accessible within the class(CAR)).
            //Protected - Properties and method are only accessible by the class of the same package (same as default) and the subclass present in any package.
            // Public - Properties and method can be accessed from anywhere.


    // class creation
    // four parts of the class creation

    // 1) Properties - characteristic of an object

    private String name;
    private String brand;
    private int yearOfMake;
    // make a component of a car
    private Driver driver;


    // 2) Constructors - use to create/instantiate an object:

    // empty constructor - create object that doesnt have any argument / parameters

    public Car(){
        //Whenever a new car is created, it will have a "Alejandro";
        this.driver = new Driver("Alejandro");
    }

    //parameterized constructors - create an object with argument/parameters.
    public Car(String name, String brand, int yearOfMake){
        this.name = name;
        this.brand = brand;
        this.yearOfMake = yearOfMake;
        this.driver = new Driver("Alejandro");

    }

    //3) getter and setter - get and set the values of each property of the object.

    // Getter - use to retrieve the values of instantiated object

    public String getName(){
        return this.name;
    }

    public String getBrand(){
        return this.brand;
    }

    public int getYearOfMake(){
        return this.yearOfMake;
    }

    public String getDriverName(){
        return this.driver.getName();
    }

    // Setter - use to change the default values of an instantiated object.

    public void setName(String name){
        this.name = name;
    }
    public void setBrand(String brand){
        this.brand = brand;
    }

    public void setDriverName(String driver){
        this.driver.setName(driver);
    }

    public void setYearOfMake(int yearOfMake){
        if (yearOfMake < 2022) {
        this.yearOfMake = yearOfMake;

        }

    }



    // 4. Methods - function of an object it can perform(action);

    public void drive(){
        System.out.println("The Car is Running");
    }


}
