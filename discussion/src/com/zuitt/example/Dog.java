package com.zuitt.example;

//child class of animal class
    // "edxtend" keyword used to inherit the properties and method of the parent class.
public class Dog extends Animal { // 1  parent class in inheritance

    // properties

    private String breed;

    // constructor

    public Dog(){
        // this will inherit the Animal() constructor.
         super();
        this.breed = "Chihuahua";

    }


    public Dog(String name, String color, String breed){
    super(name, color);
    this.breed = breed;
    }

    // getter and setter

    public String getBreed(){
        return this.breed;
    }

    public void setBreed(String breed){
        this.breed = breed;
    }

    // Methods

    public void speak(){
        System.out.println("Woof Woof");
    }



}
