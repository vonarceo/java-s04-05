package com.zuitt.example;

// interfaces implementation
//multiple implementation using interface
public class Person implements Actions, Greetings {
    public void sleep(){
        System.out.println("Zzzzz....");
    }
    public void run(){
        System.out.println("Already Running");
    }

    public void morningGreet(){
        System.out.println("Good Morning");
    }
    public void holidayGreet(){
        System.out.println("Happy holiday");
    }
}

