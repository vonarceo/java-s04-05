import com.zuitt.example.*;



public class Main {
    public static void main(String[] args) {
//        System.out.println("Hello world!");
        // class = representation of object in code : writing ode that would describe a dog
        // instance = a unique copy of the idea, made tangible: instantiating a dog named spot from the dog class.
        // object = attributes / abstract idea
        //[Encapsulation] (Model)
        //[Inheritance] acquire the properties of method and fiends of another class

        //OOP
        //OOP stands for "Object-Oriented Programming".
        // OOP is a programming model that allows developers to design software around data or objects, rather than function and logic.
// OOP Concepts
        // Object - abstract idea that represents something in the real world.
        // Example: The concept of a dog
        // Class - representation of the object using code.
        // Example: Writing a code that would describe a dog.
        // Instance - unique copy of the idea, made "physical".
        // Example: Instantiating a dog names Spot from the dog class.
// Objects
        // States and Attributes-what is the idea about?
        // Behaviors-what can idea do?
        // Example: A person has attributes like name, age, height, and weight. And a person can eat, sleep, and speak.

        // 4 pillars of oop

        //[Encapsulation]
        // a mechanism of wrapping the data (variables) and code acting on the data (methods) together as a single unit
        // "data hiding" -  the variables of a class will be hidden from other classes, and can be accessed only through the methods of their current class.
        //To achieve encapsulation:
        // variables/properties as private.
        // provide a public setter and getter function.

        // Create a Car

        Car myCar = new Car();
        myCar.drive();



        //setter
        myCar.setName("Honda");
        myCar.setBrand("Vios");
        myCar.setYearOfMake(2015);
        //getter
        System.out.println(myCar.getName());
        System.out.println(myCar.getBrand());
        System.out.println(myCar.getYearOfMake());
        System.out.println(myCar.getDriverName());

        Car myCar2 = new Car("Kia", "Picanto", 1995);

        myCar2.setYearOfMake(2021);
        myCar2.setDriverName("John Smith");

        System.out.println(myCar2.getName());
        System.out.println(myCar2.getBrand());
        System.out.println(myCar2.getYearOfMake());
        System.out.println(myCar2.getDriverName());

        // Composition and Inheritance
        // Both concepts promotes code reuse through different approach
        //"Inheritance" allows modelling an object that is a subset of another objects.
        // It defines “is a relationship”.
        // To design a class on what it is.
        //"Composition" allows modelling objects that are made up of other objects.
        // both entities are dependent on each other
        // composed object cannot exist without the other entity.
        // It defines “has a relationship”.
        // To design a class on what it does.
        // Example:
        // A car is a vehicle - inheritance
        // A car has a driver - composition

        // 2.) Inheritance
                // Can be defined as the process where one class acquires the properties and method of another class.
        // with the use of inheritance the information made manageable in hierarchical order.


        // Dog is Animal(Dog Inherits animal class)

        Dog myPet = new Dog();

        myPet.setName("Brownie");
        myPet.setColor("White");
        myPet.setBreed("Askal");
        myPet.call();
        myPet.speak();
        System.out.println(myPet.getName() + " " + myPet.getColor() + " " + myPet.getBreed());

        //3 Abstraction
            // is a process where all the logic and complexity are hidden from the users.

            // Interfaces
                //this is used to achieve total abstraction.
                // Creating Abstract classes doesnt support "multiple inheritance"
                // act as "contracts" wherein a class implements the interface should have the methods that the interface has defined in the class.

            //Person Implements Actions & Greetings

        Person child = new Person();

        child.holidayGreet();
        child.run();
        child.morningGreet();
        child.sleep();

        // 4. Polymorphism
            // Derived from the greek word: poly means "many" and morph means "forms".
                // In short "many forms"
            // this is usually done by function/method overloading.
            //two main type of polymorphism
                // static or compile time polymorphism.
                    // method with the same name, but they have a different data types and different number of arguments.

        StaticPoly myAddition = new StaticPoly();
        //original
        System.out.println(myAddition.addition(5,4));
        // overload method based on the arguments
        System.out.println(myAddition.addition(5,4,6));
        //overload method based on the data types.
        System.out.println(myAddition.addition(5.5,3.7));

                // dynamic or run time polymorphism.
                    //function is overriden by replace the definition of the method in the parent class in the child class.

                Child myChild = new Child();

                myChild.speak();

    }
}